#!/usr/bin/make
# Makefile readme (ru): <http://linux.yaroslavl.ru/docs/prog/gnu_make_3-79_russian_manual.html>
# Makefile readme (en): <https://www.gnu.org/software/make/manual/html_node/index.html#SEC_Contents>

SHELL = /bin/sh

REGISTRY_HOST = registry.gitlab.com
REGISTRY_PATH = kerbalscientist/php-docker-image/
IMAGE_NAME = 7.4-cli
IMAGES_PREFIX := $(shell basename $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST)))))
GIT_REPOSITORY_PATH = $(dir $(realpath $(lastword $(MAKEFILE_LIST))))

PUBLISH_TAGS = latest
PULL_TAG = latest

# Important: Local images naming should be in docker-compose naming style

IMAGE = $(REGISTRY_HOST)/$(REGISTRY_PATH)$(IMAGE_NAME)
IMAGE_DOCKERFILE = ./src/Dockerfile
IMAGE_CONTEXT = ./src

CONTAINER_NAME := app

docker_bin := $(shell command -v docker 2> /dev/null)

all_images = $(IMAGE)

ifeq "$(REGISTRY_HOST)" "registry.gitlab.com"
	docker_login_hint ?= "\n\
	**************************************************************************************\n\
	* Make your own auth token here: <https://gitlab.com/profile/personal_access_tokens> *\n\
	**************************************************************************************\n"
endif

.PHONY : help pull build push login test clean \
		 test-phpstan test-phpunit \
         up down restart shell install
.DEFAULT_GOAL := help

# This will output the help for each task. thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## Show this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
	@echo "\n  Allowed for overriding next properties:\n\n\
	    PULL_TAG - Tag for pulling images before building own\n\
	              ('latest' by default)\n\
	    PUBLISH_TAGS - Tags list for building and pushing into remote registry\n\
	                   (delimiter - single space, 'latest' by default)\n\n\
	  Usage example:\n\
	    make PULL_TAG='v1.2.3' PUBLISH_TAGS='latest v1.2.3 test-tag' app-push"

# --- [ Application ] -------------------------------------------------------------------------------------------------

pull: ## pull latest Docker image (from remote registry)
	-$(docker_bin) pull "$(IMAGE):$(PULL_TAG)"

build-local: pull ## build Docker image locally
	$(docker_bin) build \
	  --cache-from "$(IMAGE):$(PULL_TAG)" \
	  --tag "$(IMAGE_LOCAL_TAG)" \
	  -f $(IMAGE_DOCKERFILE) $(IMAGE_CONTEXT)

build: pull ## build Docker image
	$(docker_bin) build \
    	  --cache-from "$(IMAGE):$(PULL_TAG)" \
    	  $(foreach tag_name,$(PUBLISH_TAGS),--tag "$(IMAGE):$(tag_name)") \
    	  -f $(IMAGE_DOCKERFILE) $(IMAGE_CONTEXT);

push: build test ## tag and push Docker image into remote registry
	$(foreach tag_name,$(PUBLISH_TAGS),$(docker_bin) push "$(IMAGE):$(tag_name)";)

login: ## Log in to a remote Docker registry
	@echo $(docker_login_hint)
	$(docker_bin) login $(REGISTRY_HOST)

clean: ## Remove images from local registry
	$(foreach image,$(all_images),$(docker_bin) rmi -f $(image);)

test: ## Run test script
	$(docker_bin) run  -v "$(GIT_REPOSITORY_PATH)/test/:/test/" "$(IMAGE):$(PULL_TAG)" "/test/test.sh"

